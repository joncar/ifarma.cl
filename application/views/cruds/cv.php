<div class="row">    
    <div class="col-xs-12 well">        
        <?= $output ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        <?php if($this->router->fetch_class()=='cv'): ?>
        $("#nombre_field_box").before(añadir_field('Información personal:','info'));
        <?php endif ?>
        $("#direccion_field_box").before(añadir_field('Información de contacto:','contacto'));
        $("#fecha_nacimiento_field_box").before(añadir_field('Información personal:','personal'));
        $("#direccion_fact_field_box").before(añadir_field('Dirección de facturación:','fact'));
        $( "#field-fecha_nacimiento" ).datepicker( "option", "yearRange", "-99:+0" );
        $( "#field-fecha_nacimiento" ).datepicker( "option", "maxDate", "+0m +0d" );
        //Paso 2
        $("#nivel_titulacion_field_box").before(añadir_field('Información de formacion:','formacion'));
        $("#hasta_input_box").append(' <span class="label label-danger">Dejar en blanco si es tu empleo actual</a>');
        <?php 
            switch($this->router->fetch_method())
            {
                case 'cv_formacion':
                    echo '$("#form-button-save").parent(".form-button-box").after(\'<div class="form-button-box"><a href="'.base_url($this->router->fetch_class().'/cv_empleo/add').'" class="btn btn-large btn-success">Ir a empleos</div>\');';
                break;
                case 'cv_empleo':
                    echo '$("#form-button-save").parent(".form-button-box").after(\'<div class="form-button-box"><a href="'.base_url($this->router->fetch_class().'/cv_referencia/add').'" class="btn btn-large btn-success">Ir a las referencias</div>\');';
                break;
            }
        ?>
    });
    
    function añadir_field(text,id)
    {
        return '<div id="'+id+'" class="form-field-box odd" style="font-size:20px; margin: 0 0 20px;">'+
                    
                    '<div id="nombre_input_box" class="form-input-box">'+
                    '<b><i class="fa fa-check-circle" style="color: rgb(160, 160, 255);"></i> '+text+'</b>'+   
                    '<div class="clear"></div>'+
                '</div>';
    }
</script>