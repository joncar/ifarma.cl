<!Doctype html>
<html lang="es">
	<head>
		<title><?= empty($title)?'Enfarmacias.cl':$title ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="description" content="<?= !empty($description)?$description:'' ?>">
		<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/font-awesome.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/fonts.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; ?>                
                <?php endif; ?>                
                <!-- Respond.js proxy on external server -->                
	</head>
	<body>
            <?php $this->load->view('includes/header') ?>
            <?php $this->load->view($view) ?>
            <?php $this->load->view('includes/footer') ?> 
        </body>
</html>