<section>
        <div class="container" style="margin-top:40px;">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active"><?= img('img/banner.jpg','width:100%') ?></div>
              <div class="item"><?= img('img/banner.jpg','width:100%') ?></div>
              <div class="item"><?= img('img/banner.jpg','width:100%') ?></div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
        </div>
        <?php $this->load->view('includes/search') ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <h1>Nuevas Farmacias ! / Actualizadas !</h1>
                    <?php $this->load->view('includes/li-list-farmacias',array('farmacias'=>$this->querys->get_ultimas_farmacias())) ?>

                </div>
                <div class="col-xs-12 col-sm-8">
                    <?php echo $this->db->get_where('paginas',array('url'=>'home'))->row()->contenido; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h1>Farmacias</h1>
                    <?php $this->load->view('guia',array('farmacias'=>$this->querys->get_ultimas_farmacias(),'height'=>'300px','nosearch'=>true)) ?>
                </div>
            </div>
        </div>
</section>           