<header>
    <?php $this->load->view('includes/navheader'); ?>
    <section class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3"><a href="<?= site_url() ?>"><?= img('img/logo.png','width:100') ?></a></div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3" style="margin-top:40px;">
                <a href="<?= site_url() ?>">Inicio</a> |
                <a href="#" class="dropdown dropdown-toggle sr-only " id="dropdownMenu1" data-toggle="dropdown">Guia de farmacias <span class="caret"></span> </a>
                <ul class="dropdown-menu" id="dropdownMenu1" role="menu" aria-labelledby="dropdownMenu1">                  
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= site_url('Guia-de-farmacias') ?>">Ver Todas</a></li>
                    <li role="presentation" class="divider"></li>
                    <?php foreach($this->querys->get_ciudades(5)->result() as $c): ?>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= site_url('Farmacias-de-'.str_replace("+","-",urlencode($c->nombre)).'-'.$c->id) ?>"><?= $c->nombre ?></a></li>
                    <?php endforeach ?>
                </ul> |
                <a href="<?= site_url('ofertas-de-empleos') ?>">Trabajos / Jobs</a> |
                <a href="<?= site_url('registrarse/add') ?>">Registrate</a>                             
            </div>
    </section>                
</header>