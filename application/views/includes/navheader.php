<nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>                        
                      </div>

                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                        
                        <ul class="nav navbar-nav">
                            <li><a href="<?= $this->db->get('ajustes')->row()->facebook ?>" style="color:blue; font-size:23px"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="<?= $this->db->get('ajustes')->row()->twitter ?>" style="color:blue; font-size:23px"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="<?= site_url('pagina-de-ayuda') ?>">Ayuda</a></li>
                            <li><a href="<?= site_url('pagina-de-contactenos') ?>">Contacto</a></li>
                            <li><a href="<?= site_url('pagina-de-publicidad') ?>">Publicidad y Banners</a></li>
                            <li></li>
                            <?php if(!empty($_SESSION['user'])): ?>
                            <li><a href="<?= site_url('panel') ?>"><i class='fa fa-user'></i> <?= $_SESSION['nombre'].' '.$_SESSION['apellido'] ?></a></li>
                            <li><a href="<?= site_url('main/unlog') ?>"><i class='fa fa-power-off'></i> Salir</a></li>                                                        
                            <?php endif ?>
                        </ul>
                        <?php if(empty($_SESSION['user'])): ?>
                        <form class="navbar-form navbar-left" action="<?= base_url('main/login') ?>"  onsubmit="return validar(this)" method="post" role="search">
                          <div class="form-group">
                            <input type="text" placeholder="Email" data-val="required" id="field-usuario" name="usuario" class="form-control">
                          </div>
                          <div class="form-group">
                            <input type="password" placeholder="Contraseña" data-val="required" id="field-pass" name="pass" class="form-control">
                          </div>
                          <button type="submit" class="btn btn-primary btn-xs">Entrar</button>                           
                        </form>
                        <?php endif ?>
                      </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>