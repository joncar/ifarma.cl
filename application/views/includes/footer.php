<footer class="well">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4">
                            <h1>Acceso rápido</h1>
                            <ul>
                                <li><a href="#">Sobre nosotros</a></li>
                                <li><a href="#">Politícas y condiciones de uso</a></li>
                                <li><a href="#">Contactanos</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-4" align="center">
                            <h1>¿Tienes una farmacia?</h1> 
                            <a href="<?= site_url('registrarse/add') ?>" class="btn btn-info btn-lg">Registrate</a>
                        </div>
                        <div class="col-xs-4">
                            <h1>Recibe Novedades</h1>
                               <form role="form">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Email:</label>
                                  <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Ingresa tu email">
                                </div>                                
                                   <center><button type="submit" class="btn btn-default">Registrar email</button></center>
                              </form>
                        </div>
                    </div>
                </div>
            </footer>