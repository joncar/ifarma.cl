<ul class="list">
    <?php foreach($farmacias->result() as $f): ?>            
    <li>
        <b>Farmacia</b> <a href="<?= site_url('Farmacia-'.str_replace("+","-",urlencode($f->nombre)).'-'.$f->id) ?>"><?= $f->nombre ?></a><br/>
        <small><i>Farmacia de <a href="<?= site_url('Farmacias-de-'.str_replace("+","-",urlencode($f->ciudad_nombre)).'-'.$f->ciudad) ?>"><?= $f->ciudad_nombre ?></a>, Teléfonos <?= $f->telefonos ?></i></small><br/>
        <a href="<?= site_url('Farmacia-'.str_replace("+","-",urlencode($f->nombre)).'-'.$f->id) ?>">Enviar Email | Ver Farmacia</a>
    </li>           
    <?php endforeach ?>
</ul>