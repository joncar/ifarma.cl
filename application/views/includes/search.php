<nav id="search" class="container">
    <form action='<?= base_url("farmacias/search") ?>' onsubmit="return validar(this)" method="get" role="form">
    <div class="form-group row">                      
        <div class="col-xs-4 col-sm-2" align="right" style="color:white; margin-top:5px"><i class="fa fa-search" ></i> Buscar: </div>
        <div class="col-xs-8 col-sm-10"><input type="search" name="search" class="form-control" value="<?php if(!empty($_GET['search'])) echo $_GET['search'] ?>" placeholder="Buscar Farmacia"></div>
    </div>                    
    </form>
</nav>