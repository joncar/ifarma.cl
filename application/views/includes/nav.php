<div class="panel-group" id="accordion" style='height:800px'>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#one">
          ADMIN
        </a>
      </h4>
    </div>
    <div id="one" class="panel-collapse collapse <?= $this->router->fetch_class() == 'admin'?'in':'' ?>">
      <div class="panel-body">
        <ul class="nav nav-stacked" style="text-align:left">  
        <li class="navbar-header" align="left"><b>Admin</b></li>
        <li align="left"><a href="<?= site_url('admin/paises') ?>"><i class='fa fa-globe'></i> Paises</a></li>
        <li align="left"><a href="<?= site_url('admin/regiones') ?>"><i class='fa fa-globe'></i> Regiones</a></li>
        <li align="left"><a href="<?= site_url('admin/ciudades') ?>"><i class='fa fa-globe'></i> Ciudades</a></li>
        <li align="left"><a href="<?= site_url('admin/industrias') ?>"><i class='fa fa-globe'></i> Industrias</a></li>
        <li align="left"><a href="<?= site_url('admin/tipos_empresas') ?>"><i class='fa fa-globe'></i> Tipos de empresa</a></li>
        <li align="left"><a href="<?= site_url('admin/tipos_trabajos') ?>"><i class='fa fa-globe'></i> Tipos de trabajo</a></li>
        <li align="left"><a href="<?= site_url('admin/tipos_jornadas') ?>"><i class='fa fa-globe'></i> Tipos de jornadas</a></li>
        <li align="left"><a href="<?= site_url('admin/tipos_formacion') ?>"><i class='fa fa-globe'></i> Tipos de formacion</a></li>
        <li align="left"><a href="<?= site_url('admin/tipos_salarios') ?>"><i class='fa fa-globe'></i> Tipos de salarios</a></li>
        <li align="left"><a href="<?= site_url('admin/ciudades') ?>"><i class='fa fa-globe'></i> Ciudades</a></li>
        <li align="left"><a href="<?= site_url('admin/farmacias') ?>"><i class='fa fa-plus'></i> Farmacias</a></li>
        <li align="left"><a href="<?= site_url('admin/usuarios') ?>"><i class='fa fa-group'></i> Usuarios</a></li>
        <li align="left"><a href="<?= site_url('admin/paginas') ?>"><i class='fa fa-file'></i> Paginas</a></li>
        <li align="left"><a href="<?= site_url('admin/tipos_farmacias') ?>"><i class='fa fa-file'></i> Tipos de farmacias</a></li>
        <li align="left"><a href="<?= site_url('admin/ajustes') ?>"><i class='fa fa-wrench'></i> Ajustes generales</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#Two">
          Panel de control
        </a>
      </h4>
    </div>
    <div id="Two" class="panel-collapse collapse <?= $this->router->fetch_class() != 'admin'?'in':'' ?>">
      <div class="panel-body">
        <ul class="nav nav-stacked" style="text-align:left">  
        <li class="navbar-header" align="left"><b>Usuario</b></li>
        <li align="left"><a href="<?= site_url('usuario/datos') ?>"><i class='fa fa-user'></i> Mis datos</a></li>
        <li align="left"><a href="<?= site_url('usuario/farmacias') ?>"><i class='fa fa-plus'></i> Mis farmacias</a></li>        
        <li align="left"><a href="<?= site_url('usuario/cv') ?>"><i class='fa fa-file'></i> Mi CV</a></li>        
        <li align="left"><a href="<?= site_url('usuario/empresa') ?>"><i class='fa fa-building-o'></i> Mi empresa</a></li>        
        <li align="left"><a href="<?= site_url('usuario/empleo') ?>"><i class='fa fa-group'></i> Ofrecer empleo</a></li>        
        </ul>
      </div>
    </div>
  </div>  
</div>