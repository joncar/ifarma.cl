<div class="container" style="display: none">
    <?php $this->load->view('includes/search') ?>
    <div class="row">
        <div class="col-xs-12 well" style="margin:20px 0">
            <h1>Farmacias de <?= $farmacias->row()->ciudad_nombre ?></h1>
            <?php $this->load->view('includes/li-list-farmacias',array('farmacias'=>$farmacias)) ?>
        </div>
        
    </div>
</div>