<div align="center" class="row col-xs-12" style="margin:20px 0">    
    
    <?php if(empty($_SESSION)): ?>
    <div class='col-sm-12'>
    <?php else: ?>
    <div class='col-xs-12 col-sm-3 inverse well' style='height:100%'>
        <?php $this->load->view('includes/nav'); ?>
    </div>
    <div class='col-xs-12 col-sm-9' style='min-height:80%'>
    <?php endif ?>
    <?php if(empty($crud)): ?>
        <h1>Panel de control</h1>
    <?php else: ?>        
        <?php $this->load->view('cruds/'.$crud); ?>
    <?php endif ?>
    </div>    
        
</div>