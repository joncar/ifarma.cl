<?php $this->load->view('region') ?>
<div class="container">
    <?php if(empty($nosearch) || !$nosearch)$this->load->view('includes/search'); ?>    
    <div id="mapa" class="col-xs-12" style="<?= empty($height)?'height:500px':'height:'.$height.';' ?>"></div>  
    <div class="row" style="margin-top:20px">
        <?php foreach($this->db->get('tipos_farmacias')->result() as $t): ?>
        <div class="col-xs-4" align="center"><a href="<?= site_url('Guia-de-farmacias-'.$t->id) ?>"><?= img('files/'.$t->icono) ?></a><br/><?= $t->nombre ?></div>        
        <?php endforeach ?>
    </div>
</div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true"></script>
<script type="text/javascript">
function mapa(){
    this.map = undefined;
    this.marker = undefined;
    this.lat = '-33.411353071752266';
    this.lon = '-70.60676905000003';
    this.initialize = function() {                
        this.cargar_mapa();
    }                
    this.cargar_mapa = function()
    {         
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(this.lat,this.lon),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(document.getElementById('mapa'),mapOptions);                    
        <?php foreach($farmacias->result() as $n=>$f): 
            $coords = explode(",",$f->mapalat);
            $lat = str_replace("(",'',$coords[0]);
            $lon = str_replace(")",'',$coords[1]);
        ?>
        this.<?= 'f'.$n ?> = new google.maps.Marker({
        position: new google.maps.LatLng(<?= $lat ?>,<?= $lon ?>),
        map: this.map,           
        title: 'Ubicación',
        icon:'<?= base_url('files/'.$f->icono) ?>'});
         
        this.<?= 'info'.$n ?> = new google.maps.InfoWindow({content: '<?= $this->load->view('includes/ficha',array('fa'=>$f),TRUE) ?>'});
        google.maps.event.addListener(m.<?= 'f'.$n ?>,'click', function(){
            m.<?= 'info'.$n ?>.open(m.map,m.<?= 'f'.$n ?>);
        });
        <?php endforeach ?>
    }
}
var m = new mapa();
m.initialize();
</script>