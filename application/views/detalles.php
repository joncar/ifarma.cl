<div class="container">
    <?php $this->load->view('includes/search') ?>
    <div class="row">
        <div class="col-xs-12 col-sm-8">
            <h1>Farmacia - <?= $farmacia->nombre ?></h1>
            <div id="detalle">
                <?= $farmacia->descripcion ?>
            </div>
            <h2>Valoración de Farmacia</h2>
            <ul id="rank" class="stars" data-url="farmacias/rank">
                <li data-val="1"></li>
                <li data-val="2"></li>
                <li data-val="3"></li>
                <li data-val="4"></li>
                <li data-val="5"></li>
            </ul>
            <h2>Atención</h2>
            <ul id="rank_atencion" data-url="farmacias/rank_atencion" class="stars">
                <li data-val="1"></li>
                <li data-val="2"></li>
                <li data-val="3"></li>
                <li data-val="4"></li>
                <li data-val="5"></li>
            </ul>
            <div class="row">
                <div class="col-xs-6">
                    <h2>Dirección</h2>
                    <div class="col-xs-6"><b>Pais: </b></div>
                    <div class="col-xs-6"><?= $farmacia->farmacia_pais ?></div>
                    <div class="col-xs-6"><b>Ciudad: </b></div>
                    <div class="col-xs-6"><a href="<?= site_url('Farmacias-de-'.str_replace("+","-",urlencode($farmacia->ciudad_nombre)).'-'.$farmacia->ciudad) ?>"><?= $farmacia->ciudad_nombre ?></a></div>
                    <div class="col-xs-6"><b>Localidad: </b></div>
                    <div class="col-xs-6"><?= $farmacia->localidad ?></div>
                    <div class="col-xs-6"><b>Calle: </b></div>
                    <div class="col-xs-6"><?= $farmacia->calle ?></div>
                    <div class="row col-xs-12" style="margin:130px 0;"><?= $this->querys->draw_map($farmacia->mapalat); ?></div>                    
                </div>
                <div class="col-xs-6">
                    <h2>Contacto</h2>
                    <div class="col-xs-4"><b>Horarios: </b></div>
                    <div class="col-xs-8"><?= $farmacia->horarios ?></div>
                    <div class="col-xs-4"><b>Telefonos: </b></div>
                    <div class="col-xs-8"><?= $farmacia->telefonos ?></div>
                    <div class="col-xs-4"><b>Fax: </b></div>
                    <div class="col-xs-8"><?= $farmacia->fax ?></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4" style="margin-top:20px;">
            <div class="well">
                <b>Redes sociales/Compartir</b>
            </div>
            <?php if(!empty($farmacia->obras_sociales)): ?>
            <div class="well">
                <b>Obras Sociales</b>
                <p><?= str_replace(",","-",$farmacia->obras_sociales) ?></p>
            </div>
            <?php endif ?>
            <div class="well">
                <b>Medios de pagos</b>
                <p><?= str_replace(",","-",$farmacia->medios_pagos) ?></p>
            </div>
            <div class="well">
                <b>Contactalos</b>
                 <form role="form">
                    <div class="form-group">
                      <label for="nombre">Nombre</label>
                      <input type="text" class="form-control" id="nombre" placeholder="Coloque su nombre y apellido">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" placeholder="Coloque su email">
                    </div>
                    <div class="form-group">
                      <label for="mensaje">Mensaje</label>
                      <textarea name="mensaje" id="mensaje" placeholder="Coloque su mensaje" class="form-control"></textarea>
                    </div>
                     <div class="checkbox">
                        <label>
                          <input type="checkbox" name="copy"> Enviarme una copia
                        </label>
                      </div>
                     <center><button type="submit" class="btn btn-success">Enviar</button></center>
                  </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    drawrank(<?= $farmacia->rank ?>,"#rank")
    drawrank(<?= $farmacia->rank_atencion ?>,"#rank_atencion")
    var h = 0;
    var active = '';
    $(".stars li").mouseover(function(){
        h = $(this).attr('data-val');
        $(this).parent('ul').find('li').each(function(){
            if(parseInt($(this).attr('data-val'))<=parseInt(h))
                $(this).addClass('hover');
        });
    });
    $(".stars li").mouseout(function(){
        $(".stars li").removeClass('hover');
    });
    
    $(".stars li").click(function(){
        active = "#"+$(this).parent('ul').attr('id');
        $.post('<?= base_url() ?>'+$(this).parent('ul').attr('data-url'),{farmacia:'<?= $farmacia->id ?>',val:$(this).attr('data-val')},function(data){
            drawrank(data,active);
        })
    });
});

function drawrank(v,y)
{    
    if(v>0)
    {
        $(y).find('li').each(function(){
            if(parseInt($(this).attr('data-val'))<=v)
                $(this).addClass('active');
        });
    }
}
</script>