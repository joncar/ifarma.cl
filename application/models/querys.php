<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    function get_ciudades($limit = '')
    {
        if(!empty($limit))$this->db->limit($limit);
        $ciudades = $this->db->get_where('ciudades');
        foreach($ciudades->result() as $c=>$f){
            $this->db->where('ciudad',$f->id);
            $ciudades->row($c)->farmacias = $this->get_farmacia()->num_rows;
        }
        return $ciudades;
    }
    
    function get_ultimas_farmacias($cantidad = 5)
    {
        $this->db->limit(5);
        $this->db->order_by('id','DESC');                
        return $this->get_farmacia();
    }        
    
    function get_farmacia($id = '',$where=true)
    {
        $this->db->select('farmacias.*, paises.nombre as farmacia_pais, ciudades.nombre as ciudad_nombre,tipos_farmacias.icono');
        $this->db->join('tipos_farmacias','tipos_farmacias.id = farmacias.tipo_farmacia');
        $this->db->join('paises','paises.id = farmacias.pais');
        $this->db->join('ciudades','ciudades.id = farmacias.ciudad');
        if($where)$this->db->where('status',0);
        return !empty($id)?$this->db->get_where('farmacias',array('farmacias.id'=>$id,'status'=>0)):$this->db->get('farmacias');
    }
    
    function get_rankin($id,$table = 'rankings')
    {                
        $f = $this->db->get_where($table,array('farmacia'=>$id));
        $sum = 0;
        if($f->num_rows>0){
            foreach($f->result() as $x)
            $sum+=$x->valor;
            return round($sum/$f->num_rows,0);
        }
        else return 0;
        
    }
    
    function draw_map($coords)
    {
        $this->load->config('grocery_crud');
        $coords = explode(",",$coords);
        $lat = str_replace("(","",$coords[0]);
        $lon = str_replace(")","",$coords[1]);
        $str =  '<script src="'.$this->config->item('map_lib').'"></script>';
        $str .= '<script src="'.base_url('assets/grocery_crud/js/jquery_plugins/config/map.js').'"></script>';
        $str .= '<div id="mapa" style="width:100%; height:300px"></div>
                   <script>
                    var m = new mapa(\'mapa\',\''.$lat.'\',\''.$lon.'\');
                    m.draggable = false;
                    m.initialize();                    
                    </script>';
        return $str;
    }
}
?>
