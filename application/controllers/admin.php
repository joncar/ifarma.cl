<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		parent::index();
	}                            
        /*Cruds*/     
        public function usuarios($x = '',$y = '')
	{
            $crud = parent::usuarios($x,$y);
            $crud->columns('nombre','apellido','email','pais','region','ciudad','status','cuenta');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de usuarios';
            $this->loadView($output);
	}
        
        public function ajustes($x = '',$y = '')
	{
            $crud = parent::ajustes($x,$y);            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Ajustes generales';
            $this->loadView($output);
	}
        
        public function tipos_farmacias($x = '',$y = '')
	{
            $crud = parent::tipos_farmacias($x,$y);            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de Tipos de farmacias';
            $this->loadView($output);
	}
        
        public function paises($x = '',$y = '')
	{
            $crud = parent::paises($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de paises';
            $this->loadView($output);           
	}
        
        public function regiones($x = '',$y = '')
	{
            $crud = parent::regiones($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de regiones';
            $this->loadView($output);           
	}
        
        public function ciudades($x = '',$y = '')
	{
            $crud = parent::ciudades($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de ciudades';
            $this->loadView($output);         
	}     
        
        public function paginas($x = '',$y = '')
	{
            $crud = parent::paginas($x,$y);
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de paginas';
            $this->loadView($output);         
	}
        
        public function industrias($x = '',$y = '')
        {
            $crud = parent::industrias($x,$y);
            $crud->required_fields('nombre');            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        public function tipos_empresas($x = '',$y = '')
        {
            $crud = parent::tipos_empresas($x,$y);
            $crud->required_fields('nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        public function tipos_trabajos($x = '',$y = '')
        {
            $crud = parent::tipos_trabajos($x,$y);
            $crud->required_fields('nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        public function tipos_jornadas($x = '',$y = '')
        {
            $crud = parent::tipos_jornadas($x,$y);
            $crud->required_fields('nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        public function tipos_formacion($x = '',$y = '')
        {
            $crud = parent::tipos_formacion($x,$y);
            $crud->required_fields('nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        public function tipos_salarios($x = '',$y = '')
        {
            $crud = parent::tipos_salarios($x,$y);
            $crud->required_fields('nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
                
        public function farmacias($x = '',$y = '')
        {
            $crud = parent::farmacias($x,$y);
            $crud->required_fields('nombre','tipo_farmacia','descripcion','pais','mapalat','ciudad','localidad','calle','horarios','medios_pagos','telefonos','logo','status','user');
            $crud->set_relation('user','user','nombre');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        /*Callbacks*/        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */