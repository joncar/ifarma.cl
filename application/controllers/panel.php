<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
       
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('panel');
	}
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url());
            else
            parent::loadView($crud);
        }                
        /*Cruds*/   
        public function ajustes($x = '',$y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ajustes');
            $crud->set_subject('Ajuste');                           
            //Fields            
            //unsets            
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks            
            return $crud;     
        }
        public function usuarios($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('registro');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('region','regiones','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            $crud->set_relation_dependency('ciudad','pais','pais');
            
            //Fields
            $crud->field_type('password','password');            
            //unsets            
            $crud->unset_fields('password2');
            //Displays
            $crud->display_as('pais','Pais donde habita')
                 ->display_as('ciudad','Ciudad donde habita')                 
                 ->display_as('password','Contraseña');                        
            //Fields types            
            //Validations  
            $crud->required_fields('nombre','apellido','email','password','pais','ciudad','direccion','password2');  
            if(!empty($_POST) && !empty($y) && $this->db->get_where('user',array('id'=>$y))->row()->email != $_POST['email'])
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');            
            $crud->set_rules('password','Contraseña','required|min_length[8]');        
            //Callbacks
            $crud->callback_field('direccion',array($this,'direccionField'));
            $crud->callback_field('status',function($val){return form_dropdown('status',array(1=>'Activo',0=>'Bloqueado'),$val,'id="field-status"');});
            $crud->callback_field('cuenta',function($val){return form_dropdown('cuenta',array(0=>'Usuario',1=>'Administrador'),$val,'id="field-cuenta"');});
            $crud->callback_column('status',function($val){return $val==1?'Activo':'Bloqueado';});
            $crud->callback_column('cuenta',function($val){return $val==0?'Usuario':'Administrador';});
            $crud->callback_before_insert(array($this,'usuarios_binsertion'));                        
            $crud->callback_before_update(array($this,'usuarios_bupdate'));
            return $crud;          
	}
        
        public function paises($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('paises');
            $crud->set_subject('Pais');   
            $crud->unset_delete();
            //Fields                        
            //unsets            
            //Displays 
                                             
            //Fields types
            $crud->field_type('created','invisible');          
            $crud->field_type('modified','invisible');          
            //Validations                
            $crud->required_fields('nombre');
            return $crud;            
	}
        
        public function regiones($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('regiones');
            $crud->set_subject('Regiones'); 
            $crud->set_relation('pais', 'paises', 'nombre');
            $crud->unset_delete();
            //Fields                        
            //unsets            
            //Displays 
                                             
            //Fields types
            $crud->field_type('created','invisible');          
            $crud->field_type('modified','invisible');          
            //Validations                
            $crud->required_fields('nombre','pais');
            return $crud;            
	}
        
        public function ciudades($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ciudades');
            $crud->set_subject('Ciudades'); 
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('region','regiones','nombre');
            //Fields                        
            //unsets            
            $crud->unset_delete();
            //Displays
            //Fields types
            $crud->field_type('created','invisible');          
            $crud->field_type('modified','invisible');          
            //Validations                
            $crud->required_fields('nombre','pais','region');
            return $crud;            
	}   
        
        public function paginas($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('paginas');
            $crud->set_subject('Pagina');             
            //Fields            
            $crud->field_type('contenido','editor',array('type'=>'text_editor','editor'=>'tinymce'));            
            //unsets            
            //Displays                           
            //Fields types            
            //Validations                
            $crud->required_fields('url');            
            if((!empty($y) && !empty($_POST) && $this->db->get_where('paginas',array('id'=>$y))->row()->url != $_POST['url']) || empty($y))
            $crud->set_rules('url','URL','required|alpha_numeric|is_unique[paginas.url]');            
            return $crud;            
	}
        
        public function tipos_farmacias($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('tipos_farmacias');
            $crud->set_subject('Tipo de farmacia');                         
            //Fields                        
            
            //unsets            
            //Displays                 
            $crud->set_field_upload('icono','files');
            //Fields types            
            //Validations 
            $crud->required_fields('nombre','icono');
            return $crud;            
	}
        
        public function farmacias($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('farmacias');
            $crud->set_subject('Farmacia');             
            $crud->set_relation('tipo_farmacia','tipos_farmacias','nombre');
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');                        
            //Fields                        
            $crud->field_type('medios_pagos','tags',array('Efectivo','Deposito','Transferencia','Credito'));
            $crud->field_type('mapalat','map',array('width'=>'600px','height'=>'400px'));
            $crud->field_type('tags','tags');
            $crud->field_type('obras_sociales','tags');
            $crud->callback_column('status',function($val){return $val==0?'Publicado':'Pausado';});
            $crud->field_type('mapalon','invisible');
            $crud->callback_field('status',function($val){return form_dropdown('status',array(0=>'Publicado',1=>'Pausado'),$val,'id="field-status"');});            
            //unsets            
            //Displays     
            $crud->display_as('medios_pagos','Medios de pago'); 
            $crud->set_field_upload('logo','files');
            $crud->display_as('mapalat','Ubicación en el mapa');            
            //Fields types            
            //Validations             
            return $crud;            
	}
        
        public function cv($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('cv');
            $crud->set_table('cv');
            $crud->set_subject('CV');   
            //Relations
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('region','regiones','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            $crud->set_relation('pais_fact','paises','nombre');
            $crud->set_relation('region_fact','regiones','nombre');
            $crud->set_relation('ciudad_fact','ciudades','nombre');
            
            $crud->set_relation_dependency('region','pais','pais');
            $crud->set_relation_dependency('ciudad','region','region');            
            $crud->set_relation_dependency('region_fact','pais_fact','pais');
            $crud->set_relation_dependency('ciudad_fact','region_fact','region');
                    
            //Fields                                    
            $crud->set_field_upload('foto','files');
            $crud->field_type('sexo','enum',array('M','F'));
            $crud->field_type('estado_civil','enum',array('Soltero(a)','Casado(a)','Viudo(a)','Divorciado(a)'));
            $crud->field_type('habilidades','editor',array('type'=>'text_editor','editor'=>'markitup'));
            //unsets            
            //Displays                 
            $crud->display_as('pais_fact','Pais')
                 ->display_as('region_fact','Region')
                 ->display_as('ciudad_fact','Ciudad')
                 ->display_as('direccion_fact','Dirección')
                 ->display_as('codigo_postal_fact','Codigo Postal')
                 ->display_as('telefono_fact','Telefono');
            $crud->set_lang_string('insert_success_message','Datos guardados dirigiendo al paso 2 <script>setTimeout(function(){document.location.href="'.base_url($this->router->fetch_class().'/cv_formacion').'/add"},2000)</script>');
            //Fields types            
            //Validations             
            return $crud;            
	}
        
        public function cv_formacion($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('cv_formacion');
            $crud->set_subject('Formacion');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                 
            $crud->display_as('universidad','Centro/Universidad/Liceo/Instituto');
            //Fields types            
            
            //Validations             
            $crud->required_fields('nivel_titulacion','fecha_graduacion','universidad');
            return $crud;            
	}
        
        public function cv_empleo($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('cv_empleo');
            $crud->set_subject('Empleo');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                 
            $crud->display_as('universidad','Centro/Universidad/Liceo/Instituto');
            //Fields types            
            
            //Validations             
            $crud->required_fields('empresa','designacion','funciones','desde');
            return $crud;            
	}
        
        public function cv_referencia($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('cv_referencia');
            $crud->set_subject('Referencia');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                             
            $crud->display_as('anos','Años conociendome');
            $crud->columns('nombre');
            //Fields types            
            
            //Validations             
            $crud->required_fields('nombre','email','parentesco','anos','telefono');
            $crud->set_rules('email','Email','required|email_valid');
            return $crud;            
	}
        
        public function industrias($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dbindustrias');
            $crud->set_subject('Industria');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function tipos_empresas($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dbtipo_empresa');
            $crud->set_subject('Tipo de empresa');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function tipos_trabajos($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dbtipo_trabajo');
            $crud->set_subject('Tipo de trabajo');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function tipos_jornadas($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dbtipo_jornada');
            $crud->set_subject('Tipo de jornadas');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function tipos_formacion($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dbformacion');
            $crud->set_subject('Tipos de formacion');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function tipos_salarios($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dbtipo_salario');
            $crud->set_subject('Tipos de salarios');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function postulaciones($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('postulaciones');
            $crud->set_subject('Postulacion');   
            //Relations
            //Fields                                    
            //unsets   
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function empresa($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('empresas');
            $crud->set_subject('empresa');  
             //Relations
            $crud->set_relation('industria','dbindustrias','nombre');
            $crud->set_relation('tipo_empresa','dbtipo_empresa','nombre');
            
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('region','regiones','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            
            $crud->set_relation_dependency('region','pais','pais');
            $crud->set_relation_dependency('ciudad','region','region'); 
            
            $crud->set_field_upload('logo','files');
            $crud->required_fields('razon_social','industria','tipo_empresa','direccion','pais',
                                   'region','ciudad','logo','codigo_postal','telefono','descripcion','historia','beneficios');
            //Fields                                    
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                                     
            $crud->display_as('descripcion','Descripción de la empresa')
                 ->display_as('historia','Historia de la empresa')
                 ->display_as('beneficios','Beneficios de la empresa');
            //Fields types            
            
            //Validations                         
            return $crud;            
	}
        
        public function empleo($x = '',$y = '')
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('empleo');
            $crud->set_subject('Oferta');   
            //Relations
            $crud->set_relation('empresa','empresas','razon_social');
            $crud->set_relation('tipo_trabajo','dbtipo_trabajo','nombre');
            $crud->set_relation('tipo_jornada','dbtipo_jornada','nombre');
            $crud->set_relation('formacion_minima','dbformacion','nombre');
            $crud->set_relation('tipo_salario','dbtipo_salario','nombre');
            
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('region','regiones','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            
            $crud->set_relation_dependency('region','pais','pais');
            $crud->set_relation_dependency('ciudad','region','region');            
            //Fields                                    
            $crud->field_type('ubicacion_mapa','map');
            $crud->callback_field('status',function($val){return form_dropdown('status',array(1=>'Publico',0=>'Oculto'),$val,'id="field-status"');});
            $crud->callback_column('status',function($val){return $val==1?'Publico':'Oculto';});
            //unsets   
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();   
            //Displays                                        
            $crud->display_as('experiencia_minima','Años de Experiencia Minima');             
            $crud->display_as('status','Status de la oferta');             
            $crud->columns('empresa','nombre','interesados','status');
            //Fields types            
            $crud->callback_column('interesados',function($val,$row){return '<a href="'.base_url($this->router->fetch_class().'/postulaciones/'.$row->id).'" target="_new">'.get_instance()->db->get_where('postulaciones',array('oferta'=>$row->id))->num_rows.'</a>';});
            //Validations                         
            $crud->required_fields('nombre','empresa','tipo_trabajo','tipo_jornada',
                    'experiencia_minima','formacion_minima','salario','tipo_salario',
                    'pais','region','ciudad','descripcion','descripcion_larga'
                    ,'ubicacion_mapa');
            return $crud;            
	}
        
        /*Callbacks*/
        function usuarios_binsertion($post)
        {                                    
            $post['password'] = md5($post['password']);
            return $post;
        }  
        function usuarios_bupdate($post,$primary)
        {
            if($this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
            $post['password'] = md5($post['password']);
            return $post;
        }  
        
        function direccionField($val = '')
        {
            return form_input('direccion',$val,'id="field-direccion" placeholder="Calle/Avenida/Residencia/Casa"');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */