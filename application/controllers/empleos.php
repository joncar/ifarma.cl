<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Empleos extends Main {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($x = '',$y = '')
	{		
                if(!empty($x) && $x=='read' && !empty($y) && is_numeric($y))
                    $_SESSION['empleo'] = $y;
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('empleos');
                $crud->set_table('empleo');                
                //Relations                
                $crud->set_relation('empresa','empresas','razon_social');
                $crud->set_relation('tipo_trabajo','dbtipo_trabajo','nombre');
                $crud->set_relation('tipo_jornada','dbtipo_jornada','nombre');
                $crud->set_relation('formacion_minima','dbformacion','nombre');
                $crud->set_relation('tipo_salario','dbtipo_salario','nombre');

                $crud->set_relation('pais','paises','nombre');
                $crud->set_relation('region','regiones','nombre');
                $crud->set_relation('ciudad','ciudades','nombre');
                //Fields                                                    
                //unsets   
                $crud->unset_add()
                     ->unset_edit()                     
                     ->unset_export()
                     ->unset_print();   
                //Displays                                                        
                //Fields types            

                //Validations        
                $crud->unset_columns('user','status','ubicacion_mapa','descripcion','descripcion_larga');
                $output = $crud->render();
                $output->view = 'empleos';
                $output->crud = 'user';                
                if($x=='read'){
                $empleo = $this->db->get_where('empleo',array('id'=>$y));
                    if($empleo->num_rows>0){
                        $output->description = 'Oferta de empleo '.$empleo->row()->descripcion;
                        $output->title = $empleo->row()->nombre;
                    }
                }
                else
                {
                $output->description = 'Ofertas de empleo publicadas por las farmacias.';
                $output->title = 'Ofertas de empleo';
                }
                $this->loadView($output);
	}   
        
        function postularse()
        {
            $this->form_validation->set_rules('id','ID','required|integer|greather_than[0]');
            if($this->form_validation->run())
            {
                if($this->db->get_where('empleo',array('id'=>$this->input->post('id')))->num_rows>0)
                {
                    if($this->db->get_where('postulaciones',array('user'=>$_SESSION['user'],'oferta'=>$this->input->post('id')))->num_rows==0){
                        $this->db->insert('postulaciones',array('user'=>$_SESSION['user'],'oferta'=>$this->input->post('id'),'fecha'=>date("Y-m-d")));
                        echo $this->success("Tu postulación ha sido enviada").'<script>$("#btn-post").remove();</script>';
                    }
                    else
                    echo $this->error('Ya ha enviado su solicitud');
                }
                else
                    echo $this->error('Ocurrio un error enviando su solicitud');
            }
            else
                    echo $this->error('Ocurrio un error enviando su solicitud');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */