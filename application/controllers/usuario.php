<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Usuario extends Panel {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		parent::index();
	}                            
        /*Cruds*/     
        public function farmacias($x = '',$y = '')
        {
            $crud = parent::farmacias($x,$y);
            $crud->where('user',$_SESSION['user']);
            $crud->required_fields('nombre','tipo_farmacia','descripcion','pais','mapalat','ciudad','localidad','calle','horarios','medios_pagos','telefonos','logo','status');
            $crud->set_model('user_privilege');
            $crud->field_type('user','invisible');
            $crud->unset_columns('user');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Mis farmacias';
            $this->loadView($output);
        }
        
        function datos($x = '',$y = '')
        {
            $crud = parent::usuarios($x,$y);
            $crud->columns('nombre','apellido','email','pais','region','ciudad','status','cuenta');
            $crud->set_model('user_privilege');
            $crud->where('user.id',$_SESSION['user']);
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $crud->unset_fields('status','cuenta','password2');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de usuarios';
            $this->loadView($output);
        }
        
        function cv($x = '',$y = '')
        {
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['cv'] = $x;
                header("Location:".base_url('usuario/cv'));
            }
            else{
            $crud = parent::cv($x,$y);            
            $crud->set_model('user_privilege');
            if(!empty($_SESSION['cv']))
            {
                $crud->where('user',$_SESSION['cv']);
                $_SESSION['cv'] = null;
                $crud->unset_edit();
                
            }
            else
            $crud->where('user',$_SESSION['user']);
            //Fields
            $crud->field_type('user','invisible');
            $crud->callback_add_field('nombre',function(){return form_input('nombre',$_SESSION['nombre'],'id="field-nombre"');});
            $crud->callback_add_field('apellido',function(){return form_input('apellido',$_SESSION['apellido'],'id="field-apellido"');});
            $crud->callback_add_field('direccion',function(){return form_input('direccion',$_SESSION['direccion'],'id="field-direccion"');});
            //Unsets
            if($this->db->get_where('cv',array('user'=>$_SESSION['user']))->num_rows>0)
            $crud->unset_add();
            $crud->unset_delete()                 
                 ->unset_export();       
            $crud->unset_back_to_list();
            //Displays
            $crud->set_lang_string('form_save','Guardar e ir al paso 2');
            //Validations
            $crud->required_fields('nombre','apellido','direccion','pais','ciudad',
                    'region','codigo_postal','telefono','fecha_nacimiento',
                    'sexo','estado_civil','nacionalidad','habilidades','direccion_fact','pais_fact','region_fact','ciudad_fact',
                    'codigo_postal_fact','telefono_fact','foto');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'cv';
            $output->title = 'Añadir tu CV';
            $this->loadView($output);
            }
        }
        
        function cv_formacion($x = '',$y = '')
        {
            $crud = parent::cv_formacion($x,$y);            
            $crud->set_model('user_privilege');
            $crud->where('user',$_SESSION['user']);
            //Fields
            $crud->field_type('user','invisible');            
            //Unsets
            $crud->unset_delete()                 
                 ->unset_export();       
            $crud->unset_back_to_list();
            //Displays
            $crud->set_lang_string('form_save','Guardar y añadir otro');
            //Validations            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'cv';
            $output->title = 'Añadir tu CV';
            $this->loadView($output);
        }
        
         function cv_empleo($x = '',$y = '')
        {
            $crud = parent::cv_empleo($x,$y);            
            $crud->set_model('user_privilege');
            $crud->where('user',$_SESSION['user']);
            //Fields
            $crud->field_type('user','invisible');            
            //Unsets
            $crud->unset_delete()                 
                 ->unset_export();       
            $crud->unset_back_to_list();
            //Displays
            $crud->set_lang_string('form_save','Guardar y añadir otro');
            //Validations            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'cv';
            $output->title = 'Añadir tu CV';
            $this->loadView($output);
        }
        
         function cv_referencia($x = '',$y = '')
        {
            $crud = parent::cv_referencia($x,$y);            
            $crud->set_model('user_privilege');
            $crud->where('user',$_SESSION['user']);
            //Fields
            $crud->field_type('user','invisible');            
            //Unsets
            $crud->unset_delete()                 
                 ->unset_export();       
            $crud->unset_back_to_list();
            //Displays
            $crud->set_lang_string('form_save','Guardar y añadir otro');
            //Validations            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'cv';
            $output->title = 'Añadir tu CV';
            $this->loadView($output);
        }
        
         function empresa($x = '',$y = '')
        {
            $crud = parent::empresa($x,$y);            
            $crud->set_model('user_privilege');
            $crud->where('user',$_SESSION['user']);            
            //Fields
            $crud->field_type('user','invisible');  
            $crud->unset_columns('user');
            //Unsets
            $crud->unset_delete()                 
                 ->unset_export();       
            $crud->unset_back_to_list();
            //Displays
            $crud->set_lang_string('form_save','Publicar empresa');
            //Validations            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Registro de empresas';
            $this->loadView($output);
        }
        
        function empleo($x = '',$y = '')
        {
            $crud = parent::empleo($x,$y);            
            $crud->set_model('user_privilege');
            $crud->where('empleo.user',$_SESSION['user']);
            //Relations
            $crud->set_relation('empresa','empresas','razon_social',array('user'=>$_SESSION['user']));
            //Fields
            $crud->field_type('user','invisible');            
            
            //Unsets
            $crud->unset_delete()                 
                 ->unset_export();       
            $crud->unset_back_to_list();
            //Displays
            $crud->display_as('nombre','Titulo de la oferta');
            $crud->set_lang_string('form_save','Publicar oferta');
            //Validations            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Ofertas de empleo';
            $this->loadView($output);
        }
        
        function postulaciones($x = '',$y = '')
        {
            if(is_numeric($x))
            {
                if($this->db->get_where('empleo',array('id'=>$x,'user'=>$_SESSION['user']))->num_rows>0){
                $_SESSION['oferta'] = $x;
                header("Location:".base_url('usuario/postulaciones'));
                }
                else
                header("Location:".base_url('usuario/empleo'));
            }
            else{
            $crud = parent::postulaciones($x,$y);            
            $crud->set_model('user_privilege');            
            $crud->where('oferta',$_SESSION['oferta']);
            //Relations
            $crud->set_relation('user','user','nombre');
            $crud->set_relation('oferta','empleo','nombre');
            //Fields            
            $crud->callback_column('s'.substr(md5('user'),0,8),function($val,$row){return '<a href="'.base_url('usuario/cv/'.$row->user).'" target="_new">'.$val.'</a>';});
            //Unsets           
             
            //Displays            
            $crud->display_as('fecha','Fecha de postulación');
            //Validations            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title = 'Postulaciones';
            $this->loadView($output);
            }
        }       
        /*Callbacks*/        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */