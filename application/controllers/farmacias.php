<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Farmacias extends Main {
        
	public function __construct()
	{
		parent::__construct();                
	}
       
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('main');
	}
        
        function farmacia($val)
        {
            $id = explode("-",$val);
            $id = $id[count($id)-1];
            $farmacia = $this->querys->get_farmacia($id);
            if($farmacia->num_rows>0){
                $farmacia->row()->rank = $this->querys->get_rankin($farmacia->row()->id);
                $farmacia->row()->rank_atencion = $this->querys->get_rankin($farmacia->row()->id,'rankings_atencion');
                $this->loadView(array('view'=>'detalles','farmacia'=>$farmacia->row(),'title'=>$val,'description'=>'Farmacia '.$val.' '.strip_tags($farmacia->row()->descripcion)));
            }
            else
            $this->loadView('404');
        }
        
        function guia($id = '')
        {
            if(!empty($id))
                $this->db->where('tipo_farmacia',$id);
            $this->loadView(array('view'=>'guia','farmacias'=>$this->querys->get_farmacia(),'title'=>'Guia de farmacias','description'=>'Lista de farmacias a nivel nacional'));
        }
        
        function region($val)
        {            
            $id = explode("-",$val);
            $id = $id[count($id)-1];
            $this->db->where('ciudad',$id);
            $farmacia = $this->querys->get_farmacia();
            if($farmacia->num_rows>0){                
                $this->loadView(array('view'=>'guia','farmacias'=>$farmacia,'title'=>$val,'description'=>'Lista de farmacias de '.$val));
            }
            else
            $this->loadView('404');
        }
        
        function farmacia_exist($id)
        {
            return $this->db->get_where('farmacias',array('id'=>$id))->num_rows>0?TRUE:FALSE;
        }
        function rank()
        {
            $this->form_validation->set_rules('farmacia','Farmacia','required|integer|greather_than[0]|callback_farmacia_exist');
            $this->form_validation->set_rules('val','Val','required|integer|greather_than[0]');
            if($this->form_validation->run())
            {
                if($this->db->get_where('rankings',array('farmacia'=>$this->input->post('farmacia'),'ip'=>$_SERVER['REMOTE_ADDR']))->num_rows==0)
                $this->db->insert('rankings',array('farmacia'=>$this->input->post('farmacia'),'valor'=>$this->input->post('val'),'ip'=>$_SERVER['REMOTE_ADDR']));
                echo $this->querys->get_rankin($this->input->post('farmacia'));
            }
        }
        function rank_atencion()
        {
            $this->form_validation->set_rules('farmacia','Farmacia','required|integer|greather_than[0]|callback_farmacia_exist');
            $this->form_validation->set_rules('val','Val','required|integer|greather_than[0]');
            if($this->form_validation->run())
            {
                if($this->db->get_where('rankings_atencion',array('farmacia'=>$this->input->post('farmacia'),'ip'=>$_SERVER['REMOTE_ADDR']))->num_rows==0)
                $this->db->insert('rankings_atencion',array('farmacia'=>$this->input->post('farmacia'),'valor'=>$this->input->post('val'),'ip'=>$_SERVER['REMOTE_ADDR']));
                echo $this->querys->get_rankin($this->input->post('farmacia'),'rankings_atencion');
            }
        }
        
        function search()
        {
            if(!empty($_GET['search']))
            {
                $this->db->where("MATCH(farmacias.nombre) AGAINST ('".$_GET['search']."') AND status='0' OR farmacias.nombre LIKE '%".$_GET['search']."%' AND status='0'  OR farmacias.tags LIKE '%".$_GET['search']."%' AND status='0'");                                
                $this->loadView(array('view'=>'guia','farmacias'=>$this->querys->get_farmacia(null,FALSE)));
            }
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */