-- phpMyAdmin SQL Dump
-- version 2.6.4-pl3
-- http://www.phpmyadmin.net
-- 
-- Servidor: db533136794.db.1and1.com
-- Tiempo de generación: 11-07-2014 a las 13:10:43
-- Versión del servidor: 5.1.73
-- Versión de PHP: 5.3.3-7+squeeze20
-- 
-- Base de datos: `db533136794`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ajustes`
-- 

CREATE TABLE `ajustes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `twitter` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `ajustes`
-- 

INSERT INTO `ajustes` VALUES (1, '', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ciudades`
-- 

CREATE TABLE `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` int(11) NOT NULL,
  `region` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `ciudades`
-- 

INSERT INTO `ciudades` VALUES (1, 1, 1, 'Santiago');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cv`
-- 

CREATE TABLE `cv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `apellido` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `direccion` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pais` int(11) NOT NULL,
  `region` int(11) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `codigo_postal` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `telefono` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `estado_civil` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `nacionalidad` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `habilidades` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `direccion_fact` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pais_fact` int(11) NOT NULL,
  `region_fact` int(11) NOT NULL,
  `ciudad_fact` int(11) NOT NULL,
  `codigo_postal_fact` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `telefono_fact` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `cv`
-- 

INSERT INTO `cv` VALUES (1, 1, 'Jonathan', 'Cardozo', '5070a-1978709_866970876652306_360113233672562894_n.jpg', 'La pastora', 1, 1, 1, '1010A', '+584169677564', '1989-08-08', 'M', 'Soltero(a)', 'Venezolano', 'PHP5, MYSQL, Postgress, HTML5, CSS3, JQuery, Javascript', 'La pastora', 1, 1, 1, '1010A', '+584169677564');
INSERT INTO `cv` VALUES (2, 2, 'Claudia', 'letzabeth', '2f45e-1385149_467970413315610_1604019517_n.jpg', 'La pastora', 1, 1, 1, '1010', '02128630605', '1989-04-26', 'F', 'Soltero(a)', 'Venezolana', 'asd', 'asd', 1, 1, 1, 'asd', 'asd');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cv_empleo`
-- 

CREATE TABLE `cv_empleo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `empresa` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `designacion` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `funciones` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `cv_empleo`
-- 

INSERT INTO `cv_empleo` VALUES (1, 1, 'NYCO', 'Programador', 'Desarrollador web', '2012-02-01', '2012-04-01');
INSERT INTO `cv_empleo` VALUES (2, 1, 'FUNDABIT', 'Programador', 'Analista de sistemas', '2012-07-02', '0000-00-00');
INSERT INTO `cv_empleo` VALUES (3, 2, 'FONDOEFA', 'Analista de sistemas', 'Analista de sistemas y de base de datos', '2013-03-13', '0000-00-00');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cv_formacion`
-- 

CREATE TABLE `cv_formacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `nivel_titulacion` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `fecha_graduacion` date NOT NULL,
  `universidad` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `cv_formacion`
-- 

INSERT INTO `cv_formacion` VALUES (1, 1, 'Bachiller en ciencias', '2006-09-17', 'Colegio agustiniano San judas tadeo');
INSERT INTO `cv_formacion` VALUES (2, 1, 'Ingeniero de sistemas', '2012-10-12', 'UNEFA');
INSERT INTO `cv_formacion` VALUES (3, 2, 'Ingeniera', '2012-03-12', 'UNEFA');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `cv_referencia`
-- 

CREATE TABLE `cv_referencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `telefono` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `parentesco` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `anos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `cv_referencia`
-- 

INSERT INTO `cv_referencia` VALUES (1, 1, 'Claudia Avilez', 'letzabeth_20@hotmail.com', '0212823123213', 'Esposa', 4);
INSERT INTO `cv_referencia` VALUES (2, 1, 'Jeynson Cardozo', 'jeynson@gmail.com', '4129535236', 'Hermano', 24);
INSERT INTO `cv_referencia` VALUES (3, 2, 'Jonathan Cardozo', 'joncar.c@gmal.com', '02128630605', 'Esposo', 2);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dbformacion`
-- 

CREATE TABLE `dbformacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `dbformacion`
-- 

INSERT INTO `dbformacion` VALUES (1, 'No especificado');
INSERT INTO `dbformacion` VALUES (2, 'Secundario completo');
INSERT INTO `dbformacion` VALUES (3, 'Universitario');
INSERT INTO `dbformacion` VALUES (4, 'Otro');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dbindustrias`
-- 

CREATE TABLE `dbindustrias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- 
-- Volcar la base de datos para la tabla `dbindustrias`
-- 

INSERT INTO `dbindustrias` VALUES (1, 'Quimico/a');
INSERT INTO `dbindustrias` VALUES (2, 'Visitador Medico');
INSERT INTO `dbindustrias` VALUES (3, 'Marketing');
INSERT INTO `dbindustrias` VALUES (4, 'Ventas');
INSERT INTO `dbindustrias` VALUES (5, 'Consultor');
INSERT INTO `dbindustrias` VALUES (6, 'Abogado/Legales');
INSERT INTO `dbindustrias` VALUES (7, 'Informática');
INSERT INTO `dbindustrias` VALUES (8, 'Otro');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dbtipo_empresa`
-- 

CREATE TABLE `dbtipo_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `dbtipo_empresa`
-- 

INSERT INTO `dbtipo_empresa` VALUES (1, 'Empleador Directo');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dbtipo_jornada`
-- 

CREATE TABLE `dbtipo_jornada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `dbtipo_jornada`
-- 

INSERT INTO `dbtipo_jornada` VALUES (1, 'Tiempo completo');
INSERT INTO `dbtipo_jornada` VALUES (2, 'Medio tiempo');
INSERT INTO `dbtipo_jornada` VALUES (3, 'A definir');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dbtipo_salario`
-- 

CREATE TABLE `dbtipo_salario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `dbtipo_salario`
-- 

INSERT INTO `dbtipo_salario` VALUES (1, 'Por Semana');
INSERT INTO `dbtipo_salario` VALUES (2, 'Por Mes');
INSERT INTO `dbtipo_salario` VALUES (3, 'Por Hora');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dbtipo_trabajo`
-- 

CREATE TABLE `dbtipo_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- 
-- Volcar la base de datos para la tabla `dbtipo_trabajo`
-- 

INSERT INTO `dbtipo_trabajo` VALUES (1, 'Atención al publico');
INSERT INTO `dbtipo_trabajo` VALUES (2, 'Responsable de farmacia');
INSERT INTO `dbtipo_trabajo` VALUES (3, 'Vendedor/a de Perfumeria');
INSERT INTO `dbtipo_trabajo` VALUES (4, 'Repartidores para farmacias');
INSERT INTO `dbtipo_trabajo` VALUES (5, 'Choferes c/ vehiculo');
INSERT INTO `dbtipo_trabajo` VALUES (6, 'Marketing / Telemarketing');
INSERT INTO `dbtipo_trabajo` VALUES (7, 'Quimico Fármaco');
INSERT INTO `dbtipo_trabajo` VALUES (8, 'Farmacéutico/a Auxiliar');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `empleo`
-- 

CREATE TABLE `empleo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `empresa` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `tipo_trabajo` int(11) NOT NULL,
  `tipo_jornada` int(11) NOT NULL,
  `experiencia_minima` int(11) NOT NULL,
  `formacion_minima` int(11) NOT NULL,
  `salario` decimal(11,2) NOT NULL,
  `tipo_salario` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pais` int(11) NOT NULL,
  `region` int(11) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `descripcion` text COLLATE latin1_general_ci NOT NULL,
  `descripcion_larga` text COLLATE latin1_general_ci NOT NULL,
  `ubicacion_mapa` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `empleo`
-- 

INSERT INTO `empleo` VALUES (1, 1, 1, 'Desarrollador web', 1, 2, 2, 3, '210000.00', '3', 1, 1, 1, 'asdasdsada', 'asdasdasdsad', '(-33.45261147891096, -70.65071436250003)', 1);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `empresas`
-- 

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `razon_social` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `industria` int(11) NOT NULL,
  `tipo_empresa` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pais` int(11) NOT NULL,
  `region` int(11) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `codigo_postal` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `telefono` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `fax` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `descripcion` text COLLATE latin1_general_ci NOT NULL,
  `historia` text COLLATE latin1_general_ci NOT NULL,
  `beneficios` text COLLATE latin1_general_ci NOT NULL,
  `logo` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `empresas`
-- 

INSERT INTO `empresas` VALUES (1, 1, 'Softwopen', 7, 1, 'La pastora', 1, 1, 1, '1010A', '02128630605', '02128630605', 'Esta es mi empresa', 'Esta es mi empresa', 'Esta es mi empresa', '98c00-cam00562.jpg');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `farmacias`
-- 

CREATE TABLE `farmacias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `descripcion` text COLLATE latin1_general_ci NOT NULL,
  `tipo_farmacia` int(11) NOT NULL,
  `pais` int(11) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `localidad` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `mapalat` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `calle` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `horarios` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `medios_pagos` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `telefonos` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `fax` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `web` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `tags` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `logo` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `obras_sociales` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `nombre` (`nombre`),
  FULLTEXT KEY `descripcion` (`descripcion`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `farmacias`
-- 

INSERT INTO `farmacias` VALUES (1, 'Farmatodo Colina', 'Encuentra farmatodo ya', 1, 1, 1, 'Calle colina', '(-33.193275823258936, -70.65346094453128)', 'Calle colina', 'De 8am a 12pm y de 1pm a 4pm', 'efectivo, credito, cre', '02128630605', '', 'http://www.farmatodo.com', '', '57cce-logo.png', '', 0, 1);
INSERT INTO `farmacias` VALUES (2, 'Farmahorro', 'Farmacia 24', 2, 1, 1, 'Farmacia 1', '(-33.48926914791309, -70.67818018281253)', 'Calle 1', '24 horas', 'efectivo, credito, debito', '02128630605', '', '', 'farmacia', '3bb6d-images-soluciones-hab-2.jpg', '', 0, 1);
INSERT INTO `farmacias` VALUES (3, 'Farmacia atención', 'Farmacia atención', 1, 1, 1, 'Los andes', '(-32.82706627642378, -70.58479639375003)', 'Los andes', '24 horas', 'efectivo', '123', '123', '', '', '5b3a2-cam00562.jpg', '', 0, 2);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `paginas`
-- 

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `contenido` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

-- 
-- Volcar la base de datos para la tabla `paginas`
-- 

INSERT INTO `paginas` VALUES (1, 'nosotros', '');
INSERT INTO `paginas` VALUES (2, 'politicas', '');
INSERT INTO `paginas` VALUES (3, 'contactenos', '');
INSERT INTO `paginas` VALUES (4, 'ayuda', '<p><strong>AYUDA</strong></p>');
INSERT INTO `paginas` VALUES (5, 'home', '<p><img style="width: 100%;" src="/remuneraciones/files/2MILLONES.png" alt="" /></p>\n<p><strong>Muy Pronto: </strong>añadiremos a nuestro sitio, actividad farmacéutica, colegios de farmacéuticos, directorio de laboratorios, noticias / enlaces, y cobertura médica.</p>');
INSERT INTO `paginas` VALUES (6, 'publicidad', '<p>Detalle de publicidad y banners.</p>');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `paises`
-- 

CREATE TABLE `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `paises`
-- 

INSERT INTO `paises` VALUES (1, 'Chile');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `postulaciones`
-- 

CREATE TABLE `postulaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `oferta` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `postulaciones`
-- 

INSERT INTO `postulaciones` VALUES (1, 1, 1, '2014-07-01');
INSERT INTO `postulaciones` VALUES (2, 2, 1, '2014-07-01');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `rankings`
-- 

CREATE TABLE `rankings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `farmacia` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `ip` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `rankings`
-- 

INSERT INTO `rankings` VALUES (1, 1, 3, '186.94.211.190');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `rankings_atencion`
-- 

CREATE TABLE `rankings_atencion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `farmacia` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `ip` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `rankings_atencion`
-- 

INSERT INTO `rankings_atencion` VALUES (1, 1, 2, '186.94.211.190');
INSERT INTO `rankings_atencion` VALUES (2, 1, 3, '201.242.117.246');
INSERT INTO `rankings_atencion` VALUES (3, 2, 4, '201.242.117.246');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `regiones`
-- 

CREATE TABLE `regiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `regiones`
-- 

INSERT INTO `regiones` VALUES (1, 1, 'Santiago');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `tipos_farmacias`
-- 

CREATE TABLE `tipos_farmacias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `icono` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `tipos_farmacias`
-- 

INSERT INTO `tipos_farmacias` VALUES (1, 'Farmacias 24 horas', '81c90-blue_8.png');
INSERT INTO `tipos_farmacias` VALUES (2, 'Farmacias de emergencia', '1097a-red_8.png');
INSERT INTO `tipos_farmacias` VALUES (3, 'Farmacias Horario normal', '4f1f5-purple_8.png');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `user`
-- 

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `apellido` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `password2` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pais` int(11) NOT NULL,
  `region` int(11) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `cuenta` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `user`
-- 

INSERT INTO `user` VALUES (1, 'Jonathan', 'Cardozo', 'joncar.c@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 1, 1, 1, 'La pastora', 0, 1);
INSERT INTO `user` VALUES (2, 'Claudia', 'letzabeth', 'letzabeth_20@hotmail.com', '25d55ad283aa400af464c76d713c07ad', '', 1, 1, 1, 'La pastora', 0, 1);
INSERT INTO `user` VALUES (3, 'pablo ignacio', 'seguel franzani', 'chiledeliver@gmail.com', 'cec5576483b55b090ee10c90d9504d44', '', 1, 1, 1, 'El canelo 27 19 ', 0, 1);
